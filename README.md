# agent-test


## Installation

### Prerequisites

You need:

1. Latest GitLab instance
1. K8s cluster

Ensure GitLab can talk to your k8s cluster


### Create Namespace

`kubectl create namespace gitlab-agent`

### Create Agent record and get token

```bash
bundle exec rails console
Feature.enable(:kubernetes_agent_internal_api)

project_id = 123 # use ID of the project you created earlier
agent_name = 'my-agent' # the name for the agent. This will be the directory name for the agent's configuration
p = Project.find(project_id)
agent = Clusters::Agent.create!(project: p, name: agent_name)
token = Clusters::AgentToken.create!(agent: agent)
puts token.token # this will print the token for the agent
```


### Using the GraphQL API to create an Agent

```bash
GRAPHQL_TOKEN=<your-token>
curl 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $GRAPHQL_TOKEN" --header "Content-Type: application/json" --request POST --data "{\"query\": \"query {currentUser {nicholasklick}}\"}"
```

### Applying the secret manifest file

Once the token has been generated it needs to by applied to the Kubernetes cluster.
Run the following command to create your secret.

`kubectl create secret generic -n gitlab-agent gitlab-agent-token --from-literal=token='YOUR_AGENT_TOKEN'`

kubectl create secret generic -n gitlab-agent gitlab-agent-token --from-literal=token='wwQryRKqnGCcxtAPq7xi'

wwQryRKqnGCcxtAPq7xi


### Install the agent into a cluster



`./resources.yml`

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-agent
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: gitlab-agent
spec:
  replicas: 1
  selector:
    matchLabels:
      app: gitlab-agent
  template:
    metadata:
      labels:
        app: gitlab-agent
    spec:
      serviceAccountName: gitlab-agent
      containers:
      - name: agent
        image: "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:latest"
        args:
        - --token-file=/config/token
        - --kas-address
        - grpc://127.0.0.1:5005 # {"$openapi":"kas-address"}
        # - grpc://host.docker.internal:5005 # use this when connecting from within Docker e.g. from kind
        volumeMounts:
        - name: token-volume
          mountPath: /config
      volumes:
      - name: token-volume
        secret:
          secretName: gitlab-agent-token
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 0
      maxUnavailable: 1
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: gitlab-agent-gitops-write-all
rules:
- resources:
  - '*'
  apiGroups:
  - '*'
  verbs:
  - create
  - update
  - delete
  - patch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-agent-gitops-write-all
roleRef:
  name: gitlab-agent-gitops-write-all
  kind: ClusterRole
  apiGroup: rbac.authorization.k8s.io
subjects:
- name: gitlab-agent
  kind: ServiceAccount
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: gitlab-agent-gitops-read-all
rules:
- resources:
  - '*'
  apiGroups:
  - '*'
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-agent-gitops-read-all
roleRef:
  name: gitlab-agent-gitops-read-all
  kind: ClusterRole
  apiGroup: rbac.authorization.k8s.io
subjects:
- name: gitlab-agent
  kind: ServiceAccount
```

```bash
kubectl apply -n gitlab-agent -f ./resources.yml
```

```
$ kubectl get pods --all-namespaces
NAMESPACE     NAME                               READY   STATUS    RESTARTS   AGE
default       gitlab-agent-77689f7dcb-5skqk      1/1     Running   0          51s
kube-system   coredns-f9fd979d6-n6wcw            1/1     Running   0          14m
kube-system   etcd-minikube                      1/1     Running   0          14m
kube-system   kube-apiserver-minikube            1/1     Running   0          14m
kube-system   kube-controller-manager-minikube   1/1     Running   0          14m
kube-system   kube-proxy-j6zdh                   1/1     Running   0          14m
kube-system   kube-scheduler-minikube            1/1     Running   0          14m
kube-system   storage-provisioner                1/1     Running   0          14m
```

### Create a Agents project


`.gitlab/agents/<agent-name>/config.yml`

```yaml
gitops:
  manifest_projects:
  - id: "path-to/your-project"
```

```bash
2020-09-15_03:48:50.35961 gitlab-k8s-agent      : time="2020-09-14T23:48:50-04:00" level=info msg="GitOps: new commit" agent_id=1 commit_id=7de7e7e9ed4ba355083978cd67abf62c806da82b
```























---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# Setting up the Agent

## Getting started

There are four main steps to getting started using the Agent

1. Creating an Agent in GitLab
1. Defining a configuration directory
1. Generating and copying a Secret token used to connect to the Agent
1. Running `kubectl apply` to allow your cluster to talk to GitLab

### Creating an Agent in GitLab

Currently you can create an Agent through the [GitLab GraphQL API](../../api/graphql/reference/#clusteragent).

If you are brand new to using the GitLab GraphQL API please refer to the [Getting started with the GraphQL API page](../../api/graphql/getting_started)

Run the following command to create your Agent

```shell
  GRAPHQL_TOKEN=<your-token>
  curl 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $GRAPHQL_TOKEN" --header "Content-Type: application/json" --request POST --data "{\"query\": \"query {currentUser {name}}\"}"
```

### Defining a configuration directory

Next you will need a Git repository that will contain your Agent configuration.

#### Git Repo Layout

The minimal repository layout looks like this:

```plaintext
|- agents
   |- my_agent_1
      |- config.yaml
```

### Generating and copying a Secret token used to connect to the Agent




### Running `kubectl apply` to allow your cluster to talk to GitLab

#### Kubernetes manifest

```
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-kubernetes-agent
type: Opaque
data:
  token: <SECRET-TOKEN-HERE>
```
